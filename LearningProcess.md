### What is the Feynman Technique?
Feynman Technique suggests that, to get a good grip on a subject or concept, explain it to someone or yourself, which will be a test to your understanding also when you try to explain it in simple sentenses, it is hard to forget again.

### In this video, what was the most interesting story or idea for you?
Oakley mentioned a point which I felt really very related and interesting that is, understanding is important but only significant when combined with practice and recall.

### What are active and diffused modes of thinking?
Active or Focus Thinking, is when thoughts are colsely packed and follwing historic patterns which usually do not open to new thoughts, on the other hand, in diffused mode the brain is not so active and open to background thinking which have more scope to get creative or new approaches or thoughts.

### According to the video, what are the steps to take when approaching a new topic? Only mention the points.
1. Deconstructring the skill.
2. Learn enough to self correct.
3. Remove distractions.
4. Practice at least 20 hours.

### What are some of the actions you can take going forward to improve your learning process?
Firstly, I feel myself little proud, that I'm already following some learning techniques. After going through all the learning process modules, I clearly understood how to increase mye productivity. I am planning to follow pomodoro technique, and also four steps to learn anything described by Mr. Josh in his ted talk. Also the deep work article gave me motivation to learn things perfectly. In conclusion I am gonne follow these :

- Not think myself that I know the concept may be I can move to next topic before testing my knowledge in it.
- I will practice more and more.
- Follow pomodoro technique to control my stress.
- I already unistalled all social media (3rd step in josh's ted talk is to remove distractions).
