# Listening and Assertive Communication

### What are the steps/strategies to do Active Listening?

- Avoid getting distracted by our thoughts.
- Focus on the speaker and topic.
- Do not interrupt the other person. 
- Let them finish and then respond.
- Use phrases like notifying them, that we are interested in the topic.
- Let them know that you are listening using your body language.
- Take notes if appropriate.
- Rephrase what they said to ensure that both are inflow.

### According to Fisher's model, what are the key points of Reflective Listening? 

The key points of reflective listening are, seeking to understand the speaker's idea and telling it back to the speaker to confirm that we have understood it correctly.

### What are the obstacles in your listening process?

- Our thoughts.
- Interruptions
- Lack of focus
- When the pace of listening does not meet the speaker's pace of telling.

### What can you do to improve your listening?
- Focus while listening.
- Will not interrupt the speaker.
- Repeat what I understood, to ensure that I understood correctly.
- Take notes to ensure that I did not miss any point even a minute one.

### When do you switch to Passive communication style in your day to day life?
I will usually communicate in a passive style when I am talking to elders and seniors.

### When do you switch to Aggressive communication styles in your day to day life?
I will switch to an aggressive style when I am more depressed or stressed, and also when someone tries to demotivate or try to be negative with me. But from now I will try to handle such situations in an assertive style. 

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
There will be lots of situations where I switch to a Passive-Aggressive style of communication, like when I am not able to say no to someone or something that is concerning to me, but I need to express my feelings or needs, in such situations I will switch to this mode of communication.


### How can you make your communication assertive? 

I follow these steps to make my communication assertive,
- Learn to recognize and name my feelings and needs.
- Will be cautious with my tone, and I will make sure my tone is not passive or aggressive.
- Not wait until the situation is out of hand.
- Will tart with low-stakes situations.
