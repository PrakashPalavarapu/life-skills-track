### What kinds of behaviour cause sexual harassment?

There are three kinds of sexual harassment, 

1. Verbal : Verbal harassment includes comments on clothing or a person's body. Even making jokes or remarks which which are mean and degrading comes under this category.

2. Visual : In this kind of behaviour, sending inoppropriate posters, mails, texts, or cartoons are considered as visual harassment.

3. Physical : This is the most illegal behaviour, in this kind of harassment, the acts like sexual assault, impeding or blocking movement, inappropriate touching are included.


### What would you do in case you face or witness any incident or repeated incidents of such behaviour? / How to handle cases of harassment?

The very first thing I will tell to the person was to stop what he/she is doing. If this not works, follow the proceedure according to the organization to file a complaint.



### Explains different scenarios enacted by actors.

- "Artistic Freedom", in whcih they enacted how visual harassment is done by showing inappropriate poster.
- "Ya Gotta Keep Tring", showed how visual harassment can happen through mails.
- "the Joke's on you", making jokes on personality.
- "The legend" - shows physical harassament.
- "Odd man Out" - described Quid pro quo.

### How to behave appropriately?

- Know the limits of others
- Understand the perception of them
- and then make a boundary for your activites accordingly, such that they do not feel discomfort.
- Mailny, anything you feel inoppropriate in your heart, Just don't do it. 
- The actual intention for being in limits is not to make other discomfort. 