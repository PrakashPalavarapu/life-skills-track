# <center> Event Sourcing - Through JavaScript </center>

## Abstract
In this paper, the concept of Event-sourcing (ES) is explained through JavaScript by replicating an application made using simple HTML. Firstly, the data loss simulation is shown, where all the previous events data is replaced by the latest event data, and the paper proceeds to the simulation of a simple application with ES, followed by the benefits and drawbacks of ES. Finally do not miss the conclusion part and an extra interesting session is added.

## Introduction
In simple words, ES is a software design pattern which is capable of capturing all the events that occur in an application as a sequence of events [1]. The most commonly used approach to update, delete, create, or read the data in most of the systems is using CRUD operations, but using CRUD operations leads to data loss, and it is not automated. Fortunately, we have ES to solve this issue [2].

## Data Loss Simulation using JavaScript
Let's now see practically how data loss happens if ES is not implemented in an application. For this, I created a simple web application having two text input boxes, and a submit button, every time a button is clicked an event is generated.

Clicking the submit button is an event, the data Nama and Email will be updated every time the submit button is clicked.

1. First submit:

![alt text](https://firebasestorage.googleapis.com/v0/b/eshop-b8924.appspot.com/o/images%2Fimage.png?alt=media&token=2d2e5c0b-aa1e-48d7-a19f-c67a4ce7db8b)

2.Second submit:

![alt text](https://firebasestorage.googleapis.com/v0/b/eshop-b8924.appspot.com/o/images%2Fimage-1.png?alt=media&token=616a82b9-8752-4523-932d-d1e88cc3b55d)


## Tracking Previous Events Simulation using JavaScript

Now let's track every event, that is, let's keep the log of every Name and Email submitted, and write in output.

1. First submit:

![alt text](https://firebasestorage.googleapis.com/v0/b/eshop-b8924.appspot.com/o/images%2Fimage-2.png?alt=media&token=d9407a93-081b-4d11-aeae-fc6b69554802)

2. Second submit:

![alt text](https://media.discordapp.net/attachments/1232004531856867418/1240899603222630410/image-3.png?ex=66483d50&is=6646ebd0&hm=e9a8acf77d0ececb556adf5ff7d3ac67e527759d39dedd589388b0cb2c7378f7&=&format=webp&quality=lossless)

[Access code for above illustration](https://gitlab.com/PrakashPalavarapu/eventscourcinghtml-js.git)

## Benefits and Drawbacks

### Pros:
1. Data can be reconstituted from the event store.
2. Auditing and Root Cause Analysis.
3. Data asset for projections and predictions.

### Cons:
1. Decreases application startup speed.
2. Requires an extremely efficient network.
3. Cost to value will be an important factor.

References to this session - [3],[4]

## Commmon Misconception

There is a common misconception, that ES is a fancy name to git. But there are lots of differences that one should notice, git tracks versions of code whereas, ES tracks the events. Events can get functional when the complete code is done and deployed, that means after a version of code is concluded which is tracked by git [5].

## Conclusion 

Let's summarise everything we have learned. Event sourcing is a software design architecture where every event in an application is tracked and can be logged at any time which helps in analysing and planning business strategies, on the other hand, ES has its disadvantages like making the application slow. However, it is essential to decide whether to implement the ES in your application or not. Just think, if it's worth investing in extra code, also implementing ES makes it more complex. For a simple application where ES is not a factor to invest in, then omitting it will be a good choice.

## References
1. [A blog on Event Sourcing by Martin Flower](https://martinfowler.com/eaaDev/EventSourcing.html)
2. [Bachelor's Thesis on Event Sourcing](http://hdl.handle.net/11250/2565253)
3. [Article on ES benifits and drawbacks by Yestin Tian](https://dev.to/wfoxd/event-sourcing-benefits-and-drawbacks-4o02)
4. [RedHat article on the pros and cons of the ES architecture pattern](https://www.redhat.com/architect/pros-and-cons-event-sourcing-architecture-pattern)
5. [HackerNews Debate on git v/s ES](https://news.ycombinator.com/item?id=18706355)





<center>Hurray!! you made it to the end</center>