### What are the activities you do that make you relax - Calm quadrant?
I feel relaxed when I was engaged in sipritual activities like meditating, chanting, and listening to devotional songs.

### When do you find getting into the Stress quadrant?
I usually feel stressed when I have multiple tasks that should be completed, and also when I am ill.

### How do you understand if you are in the Excitement quadrant?
the factors that determine if I am excited are
- engaing in things more actively
- satisfied
- joy

### Paraphrase the Sleep is your Superpower video in your own words in brief. 
- sleep helps to learn better.
- remeber what we learned. 
- not sleeping causes ageing.
- maintains health of heart
- increases the immunity.
- in simple words, the shoter sleep means shorter life.


### What are some ideas that you can implement to sleep better?
These are the steps i will implement to sleep better
- avoid coffee
- maintain a sleep shedule
- and try to maintain room cool.

### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
- feels stronger
- increases focus
- long term memory is increased.
- better energy
- has immediate positive effects on brain.
- fastens the reaction time.


### What are some steps you can take to exercise more?
- I will try to do excersice 3 to 4 times a week and will make sure every execrsie session lasts for atleast 30 mins.
- I will do power walks.
- will do zumba.
- and also I will include simple physical movements like sprinting, pushups etc., 
