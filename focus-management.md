### What is Deep Work?
- working without distractions and complete focus is called deep work.

### According to author how to do deep work properly, in a few points?
- Schedule the distractions and limit them.
- develop a deep work ritual, we can start it as a tiny habit.
- maintain sleep.

### How can you implement the principles in your day to day life?
- I will schedule my distractions like watching social media, and will minimise my distractions.
- will start doing regular deep work practices.
- I will maintain proper sleep  


### What are the dangers of social media, in brief?
- addictive
- disturbs the deep work ability
- we feel lonely and isolated
- more chances of anxiety-related disorders