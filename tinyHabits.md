### what was the most interesting story or idea for you? (BJ Fogg video)
- the concept of doing little things after any of our routine makes that tinay habit habitual

### How can you use B = MAP to make making new habits easier? What are M, A and P.
- M, A and P are three factors that helps us to achieve tiny habits. Motivation (M), which is the desire that one should carry to start (or continue) the tiny tasks, and A refers the Ability to do the task. P (Promt) which means a reminder to do the tasks. These are the factors that we should adjust.

### Why it is important to "Shine" or Celebrate after each successful completion of habit?
celebrating after each completion of habit fills the pride in us and and also helps us to motivate to continue for the next tasks.

### what was the most interesting story or idea for you? (1% Better Every Day Video)
The difference between having clarity and having motivation, where many people think they are lack in motivation but they lack in clarity. If a person has clarity on what to achieve and when to acheive will have high motivation factors.

### What is the book's perspective about Identity?
The ultimate form of intrinsic motivation is when a habit becomes part of our identity.

### Write about the book's perspective on how to make a habit easier to do?
with the four steps mentioned in the book, we can achive to make a habit easier. The four steps are 
-- start the habit (cue).
-- Motivation to do (craving).
-- The action or habit we perform (response).
-- And the reward is the end goal (reward).

### Write about the book's perspective on how to make a habit harder to do?
by making habit obvious, making it attractive, making it easy , and final one is to making it immediatly reward.

### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
The one habit I want to habituate is to wake up early and sleep early, to achiveve this I will reshedule my whole day plan and I will not try to acheive this from day one. 

### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
The one thing I want to eliminate is procatination, when ever I feel like procatinate the tnings, I will think of the negative effects that gonna come because of postponing it, and I will keep a rule that if I postpone an important thing, I will not talk to my family on that day which is like a punishment to me.
  